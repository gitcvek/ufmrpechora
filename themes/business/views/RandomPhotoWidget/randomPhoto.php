<?php 
$this->registerCssFile('photogallery-random-widget.css');
?>
<div class="b-photogallery-random-widget">
<a class="photogallery-link" href="<? echo $galleryLink ?>">Фотогалерея</a>
  <?php if ($photo): ?>
  <a href="<?=$photoLink?>"><img src="<?=$photo->getUrlPath()?>"></a>
  <?php endif; ?>
</div>