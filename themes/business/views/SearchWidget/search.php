<form action="<?=Yii::app()->createUrl(SearchModule::ROUTE_SEARCH_VIEW)?>" class="navbar-form navbar-right" role="search">
 <div class="input-group">
      <input type="text" id="query" name="query" class="form-control" placeholder="Найти">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
      </span>
    </div>
</form>